package brbid.com.br.brbid.listar;

class Presenter {

    private ListarInterfaces.View view;
    private ListarInterfaces.Model model;

    Presenter(ListarInterfaces.View view, ListarInterfaces.Model model) {
        this.view = view;
        this.model = model;
    }

    /**
     * Pega os carros do banco e passa para o CarrosAdapter atualizar. Deve ser chamado para atualizar a lista após mudanças no banco.
     */
    private void atualizarCarros() {
        view.getAdapter().atualizarCarros(model.getCarros());
    }

    void deleteCarro(int id) {
        model.deleteCarro(id);
        atualizarCarros();
    }

    void onNotHidden() {
        atualizarCarros();
    }

}
