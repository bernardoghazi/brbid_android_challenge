package brbid.com.br.brbid.data;

import java.util.List;

import brbid.com.br.brbid.model.Carro;

/**
 * Interface para comunicação com o banco de dados.
 */
public interface DAO {
    /**
     * Insere o Carro (@param c) no banco de dados.
     * Retorna o id usado no banco.
     */
    Long addCarro(Carro c);

    /**
     * Remove do banco o Carro com o id passado (@param id).
     */
    void deleteCarro(int id);

    /**
     * Retorna todos os carros cadastrados no banco.
     */
    List<Carro> getCarros();

    /**
     * Retorna do banco o Carro cadastrado com o id passado (@param id).
     */
    Carro getCarro(Integer id);

    /**
     * Atualiza o Carro que tem o id (@param id) no banco de dados com as características do @param c.
     */
    void updateCarro(Integer id, Carro c);
}
