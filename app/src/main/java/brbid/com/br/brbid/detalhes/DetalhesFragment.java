package brbid.com.br.brbid.detalhes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import brbid.com.br.brbid.R;
import brbid.com.br.brbid.data.DAO;
import brbid.com.br.brbid.data.SQLiteService;
import brbid.com.br.brbid.host.HostActivity;
import brbid.com.br.brbid.model.Carro;

public class DetalhesFragment extends Fragment {

    private Integer id;

    EditText modeloEt;
    EditText corEt;
    EditText marcaEt;
    EditText anoEt;
    EditText placaEt;
    Button editarBt;
    Button salvarBt;
    Button cancelarBt;
    private Carro carroInicial;

    Context context;
    Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detalhes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        context = getActivity();
        DAO dao = new SQLiteService(context);
        Model model = new Model(dao);
        presenter = new Presenter(model);

        modeloEt = view.findViewById(R.id.modeloEtDetalhes);
        corEt = view.findViewById(R.id.corEtDetalhes);
        marcaEt = view.findViewById(R.id.marcaEtDetalhes);
        anoEt = view.findViewById(R.id.anoEtDetalhes);
        placaEt = view.findViewById(R.id.placaEtDetalhes);
        editarBt = view.findViewById(R.id.editarBtDetalhes);
        salvarBt = view.findViewById(R.id.salvarBtDetalhes);
        cancelarBt = view.findViewById(R.id.cancelarBtDetalhes);

        editarBt.setOnClickListener(v -> {
            desbloquearInput();
            habilitarSalvar();
        });

        cancelarBt.setOnClickListener(v -> {
            bloquearInput();
            desabilitarSalvar();
            desfazerModificacoes();
        });

        salvarBt.setOnClickListener(v -> {
            Carro carro = new Carro();
            carro.setModelo(modeloEt.getText().toString());
            carro.setCor(corEt.getText().toString());
            carro.setMarca(marcaEt.getText().toString());
            carro.setAno(anoEt.getText().toString());
            carro.setPlaca(placaEt.getText().toString());
            presenter.updateCarro(id, carro);

            Toast.makeText(context, "Carro editado", Toast.LENGTH_SHORT).show();
            esconderDetalhe();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        //Quando este fragment fica visível:
        //  Carrega o carro que foi selecionado na lista a partir do id passado.
        //  Habilita o botão 'up' para retornar à lista.
        if (!hidden) {
            ((AppCompatActivity) context).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Bundle bundle = getArguments();
            id = bundle.getInt("id");
            bloquearInput();
            desabilitarSalvar();
            Carro carro = presenter.getCarro(id);
            atualizarConteudo(carro);
        } else {
            ((AppCompatActivity) context).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        esconderDetalhe();
        return true;
    }

    //Desfaz qualuqer modificação feita nos inputs.
    private void desfazerModificacoes() {
        atualizarConteudo(carroInicial);
    }

    private void habilitarSalvar() {
        ((HostActivity) context).setTitulo("EDITAR");
        editarBt.setVisibility(View.INVISIBLE);
        salvarBt.setVisibility(View.VISIBLE);
        cancelarBt.setVisibility(View.VISIBLE);
    }

    private void desabilitarSalvar() {
        ((HostActivity) context).setTitulo("DETALHES");
        editarBt.setVisibility(View.VISIBLE);
        salvarBt.setVisibility(View.INVISIBLE);
        cancelarBt.setVisibility(View.INVISIBLE);
    }

    private void bloquearInput() {
        modeloEt.setInputType(InputType.TYPE_NULL);
        corEt.setInputType(InputType.TYPE_NULL);
        marcaEt.setInputType(InputType.TYPE_NULL);
        anoEt.setInputType(InputType.TYPE_NULL);
        placaEt.setInputType(InputType.TYPE_NULL);
    }

    private void desbloquearInput() {
        modeloEt.setInputType(InputType.TYPE_CLASS_TEXT);
        corEt.setInputType(InputType.TYPE_CLASS_TEXT);
        marcaEt.setInputType(InputType.TYPE_CLASS_TEXT);
        anoEt.setInputType(InputType.TYPE_CLASS_TEXT);
        placaEt.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    //Passa as características do @param carro para os inputs e torna esses valores o 'padrão'.
    private void atualizarConteudo(Carro carro) {
        modeloEt.setText(carro.getModelo());
        corEt.setText(carro.getCor());
        marcaEt.setText(carro.getMarca());
        anoEt.setText(carro.getAno());
        placaEt.setText(carro.getPlaca());

        carroInicial = carro.duplicate();
    }

    private void esconderDetalhe() {
        FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
        Fragment detalhesFragment = fragmentManager.findFragmentById(R.id.detalhesFragment);
        Fragment listarFragment = fragmentManager.findFragmentById(R.id.listarFragment);
        FragmentTransaction transactionListar = fragmentManager.beginTransaction();
        transactionListar.setCustomAnimations(R.anim.fade_in_slide_up, R.anim.fade_out_slide_up);
        transactionListar.hide(detalhesFragment);
        transactionListar.show(listarFragment);
        transactionListar.commit();
    }
}









