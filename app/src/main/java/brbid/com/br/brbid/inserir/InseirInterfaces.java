package brbid.com.br.brbid.inserir;

import brbid.com.br.brbid.model.Carro;

public interface InseirInterfaces {

    interface View {
        void carroSalvo();

        void clearView();

        void cadastroIncompleto();
    }



    interface Model {
        /**
         * Insere o Carro (@param carro) no banco de dados.
         */
        void inserirCarro(Carro carro);
    }
}
