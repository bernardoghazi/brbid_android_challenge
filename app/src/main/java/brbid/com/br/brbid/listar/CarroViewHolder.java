package brbid.com.br.brbid.listar;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import brbid.com.br.brbid.R;
import brbid.com.br.brbid.model.Carro;

class CarroViewHolder extends RecyclerView.ViewHolder {

    private View itemView;
    private final TextView textoTv;
    private final ImageButton deleteIb;
    private FragmentManager fragmentManager;
    private ListarFragment fragment;
    private Fragment detalhesFragment;
    private Fragment listarFragment;

    CarroViewHolder(Context contexto, View itemView, ListarFragment fragment) {
        super(itemView);
        this.itemView = itemView;
        textoTv = itemView.findViewById(R.id.itemTextoTv);
        deleteIb = itemView.findViewById(R.id.itemDeleteIb);
        fragmentManager = ((AppCompatActivity) contexto).getSupportFragmentManager();
        this.fragment = fragment;
        detalhesFragment = fragmentManager.findFragmentById(R.id.detalhesFragment);
        listarFragment = fragmentManager.findFragmentById(R.id.listarFragment);
    }

    void bind(Carro carro) {
        StringBuilder sb = new StringBuilder();
        sb.append(carro.getModelo())
                .append(" ")
                .append(carro.getCor())
                .append(" ")
                .append("(")
                .append(carro.getAno())
                .append(")");
        textoTv.setText(sb.toString());

        //Passa o id do carro selecionado para o fragment 'Detalhes'.
        itemView.setOnClickListener(v -> {
            if (detalhesFragment.isHidden()) {
                Bundle bundle = new Bundle();
                bundle.putInt("id", carro.get_id());
                detalhesFragment.setArguments(bundle);
                FragmentTransaction transactionDetalhes = fragmentManager.beginTransaction();
                transactionDetalhes.setCustomAnimations(R.anim.fade_in_slide_up, R.anim.fade_out_slide_up);
                transactionDetalhes.show(detalhesFragment);
                transactionDetalhes.hide(listarFragment);
                transactionDetalhes.commit();
            }
        });

        deleteIb.setOnClickListener(v -> {
            fragment.deleteCarroClicado(carro.get_id());
        });
    }
}