package brbid.com.br.brbid.model;

public class Carro {
    private Integer _id;
    private String modelo;
    private String cor;
    private String marca;
    private String ano;
    private String placa;
    private String timestamp;
    private String lastModified;

    public Carro() {
    }

    public Carro(String modelo, String cor, String marca, String ano, String placa) {
        this.modelo = modelo;
        this.cor = cor;
        this.marca = marca;
        this.ano = ano;
        this.placa = placa;
    }

    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getLastModified() {
        return lastModified;
    }

    //Retorna uma cópia do Carro.
    public Carro duplicate() {
        Carro duplicado = new Carro();
        duplicado.setModelo(this.getModelo());
        duplicado.setCor(this.getCor());
        duplicado.setMarca(this.getMarca());
        duplicado.setAno(this.getAno());
        duplicado.setPlaca(this.getPlaca());
        return duplicado;
    }
}
