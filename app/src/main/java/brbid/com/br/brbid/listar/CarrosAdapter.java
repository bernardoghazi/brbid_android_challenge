package brbid.com.br.brbid.listar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import brbid.com.br.brbid.R;
import brbid.com.br.brbid.model.Carro;

class CarrosAdapter extends RecyclerView.Adapter<CarroViewHolder> {

    private Context context;
    private List<Carro> carros;
    private ListarFragment fragment;

    CarrosAdapter(Context context, List<Carro> carros, ListarFragment fragment) {
        this.context = context;
        this.carros = carros;
        this.fragment = fragment;

        setHasStableIds(true);
    }

    @NonNull
    @Override
    public CarroViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.list_item, parent, false);
        return new CarroViewHolder(context, itemView, fragment);
    }

    @Override
    public void onBindViewHolder(@NonNull CarroViewHolder holder, int position) {
        Carro carro = carros.get(position);
        holder.bind(carro);
    }

    @Override
    public int getItemCount() {
        return carros.size();
    }

    @Override
    public long getItemId(int position) {
        return carros.get(position).get_id();
    }

    /**
     * Recebe nova lista e atualiza os carros.
     */
    public void atualizarCarros(List<Carro> newCarros) {
        this.carros.clear();
        this.carros.addAll(newCarros);
        notifyDataSetChanged();
    }
}
