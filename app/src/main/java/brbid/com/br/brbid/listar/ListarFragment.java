package brbid.com.br.brbid.listar;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import brbid.com.br.brbid.R;
import brbid.com.br.brbid.data.DAO;
import brbid.com.br.brbid.data.SQLiteService;
import brbid.com.br.brbid.host.HostActivity;
import brbid.com.br.brbid.model.Carro;

public class ListarFragment extends Fragment implements ListarInterfaces.View {

    Context context;
    Presenter presenter;
    RecyclerView recyclerView;
    private List<Carro> carros = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_listar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        context = getActivity();
        DAO dao = new SQLiteService(context);
        Model model = new Model(dao);
        presenter = new Presenter(this, model);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new CarrosAdapter(context, carros, this));

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            ((HostActivity) context).setTitulo("CONSULTAR CARROS");
            presenter.onNotHidden();
        }
    }

    void deleteCarroClicado(int id) {
        new AlertDialog.Builder(context)
                .setTitle("APAGAR CARRO?")
                .setMessage("Você tem certeza que quer apagar este carro? Essa operação é definitiva.")
                .setPositiveButton("CONFIRMAR", (dialog, which) -> {
                    presenter.deleteCarro(id);
                })
                .setNegativeButton("CANCELAR", (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }


    @Override
    public CarrosAdapter getAdapter() {
        return (CarrosAdapter) recyclerView.getAdapter();
    }
}