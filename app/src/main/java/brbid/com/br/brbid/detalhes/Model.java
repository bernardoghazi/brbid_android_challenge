package brbid.com.br.brbid.detalhes;

import brbid.com.br.brbid.data.DAO;
import brbid.com.br.brbid.model.Carro;

public class Model implements DetalhesInterfaces.Model {

    private DAO dao;

    public Model(DAO dao) {
        this.dao = dao;
    }

    @Override
    public void updateCarro(int id, Carro carro) {
        dao.updateCarro(id, carro);
    }

    @Override
    public Carro getCarro(int id) {
        return dao.getCarro(id);
    }
}