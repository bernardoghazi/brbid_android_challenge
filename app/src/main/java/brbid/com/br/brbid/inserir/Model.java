package brbid.com.br.brbid.inserir;

import android.content.Context;

import brbid.com.br.brbid.data.DAO;
import brbid.com.br.brbid.data.SQLiteService;
import brbid.com.br.brbid.model.Carro;

public class Model implements InseirInterfaces.Model {

    private DAO dao;

    Model(Context context) {
        dao = new SQLiteService(context);
    }

    @Override
    public void inserirCarro(Carro carro) {
        dao.addCarro(carro);
    }
}
