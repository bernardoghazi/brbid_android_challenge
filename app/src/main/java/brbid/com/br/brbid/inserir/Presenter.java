package brbid.com.br.brbid.inserir;

import brbid.com.br.brbid.model.Carro;

public class Presenter {

    private InseirInterfaces.View view;
    private InseirInterfaces.Model model;

    Presenter(InseirInterfaces.View view, InseirInterfaces.Model model) {
        this.view = view;
        this.model = model;
    }

    void salvar(String modelo, String cor, String marca, String ano, String placa) {
        if (modelo.isEmpty() || cor.isEmpty() || marca.isEmpty() || ano.isEmpty() || placa.isEmpty()) {
            view.cadastroIncompleto();
        } else {
            Carro carro = new Carro(modelo, cor, marca, ano, placa);
            model.inserirCarro(carro);
            view.carroSalvo();
            view.clearView();
        }
    }
}
