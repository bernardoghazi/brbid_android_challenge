package brbid.com.br.brbid.data;

import android.provider.BaseColumns;

/**
 * Classe que modela a tabela CARRO no banco de dados.
 */
public class CarroContract {

    public static final class CarroEntry implements BaseColumns {
        public static final String TABLE_NAME = "carros";
        public static final String CLN_ID = "_id";
        public static final String CLN_MODELO = "modelo";
        public static final String CLN_COR = "cor";
        public static final String CLN_MARCA = "marca";
        public static final String CLN_ANO = "ano";
        public static final String CLN_PLACA = "placa";
        public static final String CLN_TIMESTAMP = "timestamp";
        public static final String CLN_LAST_MODIFIED = "lastModified";
    }

}
