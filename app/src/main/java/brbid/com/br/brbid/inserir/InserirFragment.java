package brbid.com.br.brbid.inserir;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import brbid.com.br.brbid.R;
import brbid.com.br.brbid.host.HostActivity;

public class InserirFragment extends Fragment implements InseirInterfaces.View {

    Context context;

    EditText modeloEt;
    EditText corEt;
    EditText marcaEt;
    EditText anoEt;
    EditText placaEt;
    Button salvarBt;
    Button limparBt;

    Presenter presenter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inserir, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        context = getActivity();

        modeloEt = view.findViewById(R.id.modeloEt);
        corEt = view.findViewById(R.id.corEt);
        marcaEt = view.findViewById(R.id.marcaEt);
        anoEt = view.findViewById(R.id.anoEt);
        placaEt = view.findViewById(R.id.placaEt);
        salvarBt = view.findViewById(R.id.salvarBt);
        limparBt = view.findViewById(R.id.limparBt);

        InseirInterfaces.Model model = new Model(context);
        presenter = new Presenter(this, model);


        salvarBt.setOnClickListener(v -> {
            String modelo = modeloEt.getText().toString();
            String cor = corEt.getText().toString();
            String marca = marcaEt.getText().toString();
            String ano = anoEt.getText().toString();
            String placa = placaEt.getText().toString();
            presenter.salvar(modelo, cor, marca, ano, placa);
        });

        limparBt.setOnClickListener(v -> {
            clearView();
        });
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            ((HostActivity) context).setTitulo("INSERIR CARRO");
        }
    }

    @Override
    public void carroSalvo() {
        Toast.makeText(context, "Carro salvo", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearView() {
        modeloEt.setText("");
        corEt.setText("");
        marcaEt.setText("");
        anoEt.setText("");
        placaEt.setText("");
    }

    @Override
    public void cadastroIncompleto() {
        Toast.makeText(context, "Favor preencher o cadastro completo", Toast.LENGTH_SHORT).show();
    }
}









