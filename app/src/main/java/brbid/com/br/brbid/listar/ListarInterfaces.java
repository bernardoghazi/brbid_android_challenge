package brbid.com.br.brbid.listar;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import brbid.com.br.brbid.model.Carro;

public interface ListarInterfaces {

    interface View {

        CarrosAdapter getAdapter();
    }



    interface Model {

        List<Carro> getCarros();

        void deleteCarro(int id);
    }
}
