package brbid.com.br.brbid.detalhes;


import brbid.com.br.brbid.model.Carro;

class Presenter {

    private Model model;

    Presenter(Model model) {
        this.model = model;
    }

    void updateCarro(int id, Carro carro) {
        model.updateCarro(id, carro);
    }

    Carro getCarro(int id) {
        return model.getCarro(id);
    }
}
