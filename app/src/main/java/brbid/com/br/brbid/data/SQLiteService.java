package brbid.com.br.brbid.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import brbid.com.br.brbid.data.CarroContract.CarroEntry;
import brbid.com.br.brbid.model.Carro;

/**
 * Classe que implementa DAO e persiste objetos do tipo Carro em um banco de dados SQQLite.
 */
public class SQLiteService implements DAO {

    private DbHelper dbHelper;
    private SQLiteDatabase db;

    public SQLiteService(Context context) {
        dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    @Override
    public Long addCarro(Carro c) {
        ContentValues cv = new ContentValues();
        cv.put(CarroEntry.CLN_MODELO, c.getModelo());
        cv.put(CarroEntry.CLN_COR, c.getCor());
        cv.put(CarroEntry.CLN_MARCA, c.getMarca());
        cv.put(CarroEntry.CLN_ANO, c.getAno());
        cv.put(CarroEntry.CLN_PLACA, c.getPlaca());

        return db.insert(CarroEntry.TABLE_NAME, null, cv);

    }

    @Override
    public void deleteCarro(int id) {
        db.delete(CarroEntry.TABLE_NAME, CarroEntry.CLN_ID + "=" + id, null);
    }


    @Override
    public List<Carro> getCarros() {
        Cursor cursor = db.query(
                CarroEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                CarroEntry.CLN_TIMESTAMP
        );

        List<Carro> lista;
        lista = cursorToList(cursor);
        cursor.close();
        return lista;
    }


    @Override
    public Carro getCarro(Integer id) {
        String[] sArray = new String[]{Integer.toString(id)};
        Cursor cursor = db.query(
                CarroEntry.TABLE_NAME,
                null,
                CarroEntry._ID + "=?",
                sArray,
                null,
                null,
                CarroEntry.CLN_TIMESTAMP
        );

        List<Carro> lista;
        lista = cursorToList(cursor);
        cursor.close();
        return lista.get(0);
    }


    @Override
    public void updateCarro(Integer id, Carro c) {
        String[] idArray = {id.toString()};
        ContentValues cv = new ContentValues();

        cv.put(CarroEntry.CLN_MODELO, c.getModelo());
        cv.put(CarroEntry.CLN_COR, c.getCor());
        cv.put(CarroEntry.CLN_MARCA, c.getMarca());
        cv.put(CarroEntry.CLN_ANO, c.getAno());
        cv.put(CarroEntry.CLN_PLACA, c.getPlaca());

        db.update(
                CarroEntry.TABLE_NAME,
                cv,
                CarroEntry.CLN_ID + "=?",
                idArray);
    }


    private List<Carro> cursorToList(Cursor cursor) {
        List<Carro> lista = new ArrayList<>();
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            Carro carro = new Carro();
            carro.set_id(cursor.getInt(cursor.getColumnIndex(CarroEntry.CLN_ID)));
            carro.setModelo(cursor.getString(cursor.getColumnIndex(CarroEntry.CLN_MODELO)));
            carro.setCor(cursor.getString(cursor.getColumnIndex(CarroEntry.CLN_COR)));
            carro.setMarca(cursor.getString(cursor.getColumnIndex(CarroEntry.CLN_MARCA)));
            carro.setAno(cursor.getString(cursor.getColumnIndex(CarroEntry.CLN_ANO)));
            carro.setPlaca(cursor.getString(cursor.getColumnIndex(CarroEntry.CLN_PLACA)));
            lista.add(carro);
        }
        return lista;
    }



    public void closeDbAndHelper() {
        dbHelper.close();
        db.close();
    }
}




//public class dbService {
//
//    private final String LOG_TAG = "bernardo";
//    private app.quiQs.data.dbHelper dbHelper;
//    private SQLiteDatabase db;
//
//
//    public dbService(Context context) {
//        dbHelper = new dbHelper(context);
//        db = dbHelper.getWritableDatabase();
//    }
//
//
//    public void updateQStatus(String momentoDeCriacao, String status) {
//        String[] momentoDeCriacaoArray = {momentoDeCriacao};
//        ContentValues cv = new ContentValues();
//        cv.put(QsContract.QsEntry.COLUMN_STATUS, status);
//        int resultado = db.update(QsContract.QsEntry.TABLE_NAME, cv, QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO + "=?",
//                momentoDeCriacaoArray);
//    }
//
//
//    public void updateTimeQ(int id, long timeQ) {
//        String[] idArray = {Integer.toString(id)};
//        ContentValues cv = new ContentValues();
//        cv.put(QsContract.QsEntry.COLUMN_TIME_Q, timeQ);
//        int resultado = db.update(QsContract.QsEntry.TABLE_NAME, cv, QsContract.QsEntry._ID + "=?",
//                idArray);
//    }
//
//
//    public void updateNotificationActive(int id, String value) {
////        Log.d("meubroadcast", "updateNotificationActive chamado");
//        QObject qObj = getQFromId(id);
//        String notificationActiveDb = qObj.getNotificationActive();
//
//        if (!value.equals(notificationActiveDb)) {//só modifica o valor na db se ele for diferente do que já está lá. Não muda o mLastModified à toa.
//            String[] idArray = {Integer.toString(id)};
//            ContentValues cv = new ContentValues();
//            cv.put(QsContract.QsEntry.COLUMN_NOTIFICATION_ACTIVE, value);
//            int resultado = db.update(QsContract.QsEntry.TABLE_NAME, cv, QsContract.QsEntry._ID + "=?", idArray);
////            Log.d("meubroadcast", "updateNotificationActive db.update chamado");
//        }
//    }
//
//
//    public void updateQ(String momentoDeCriacao, QObject Q) {
//        String[] momentoDeCriacaoArray = {momentoDeCriacao};
//        ContentValues cv = new ContentValues();
//
//        cv.put(QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO, Q.getMomentoDeCriacao());
//        cv.put(QsContract.QsEntry.COLUMN_TITULO, Q.getTitulo());
//        cv.put(QsContract.QsEntry.COLUMN_DESCRICAO, Q.getDescricao());
//        cv.put(QsContract.QsEntry.COLUMN_AUDIO, Q.getAudio());
//        cv.put(QsContract.QsEntry.COLUMN_PRIORITY, Q.getPriority());
//        cv.put(QsContract.QsEntry.COLUMN_TIME_Q, Q.getTimeQ());
//        cv.put(QsContract.QsEntry.COLUMN_TIPO_TIME_Q, Q.getTipoTimeQ());
//        cv.put(QsContract.QsEntry.COLUMN_LAT_LOCALE_Q, Q.getLatLocaleQ());
//        cv.put(QsContract.QsEntry.COLUMN_LON_LOCALE_Q, Q.getLonLocaleQ());
//        cv.put(QsContract.QsEntry.COLUMN_TIPO_LOCALE_Q, Q.getTipoLocaleQ());
//        cv.put(QsContract.QsEntry.COLUMN_STATUS, Q.getStatus());
//        cv.put(QsContract.QsEntry.COLUMN_NOTIFICATION_ACTIVE, Q.getNotificationActive());
//
//        int resultado = db.update(QsContract.QsEntry.TABLE_NAME, cv, QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO + "=?",
//                momentoDeCriacaoArray);
//    }
//
//
//    public boolean removeQ(String momentoDeCriacao) {
//        return db.delete(QsContract.QsEntry.TABLE_NAME, QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO + "=" + momentoDeCriacao, null) > 0;
//    }
//
//
////    public Cursor getAllQs() {
////        return db.query(
////                QsContract.QsEntry.TABLE_NAME,
////                null,
////                null,
////                null,
////                null,
////                null,
////                QsContract.QsEntry.COLUMN_TIMESTAMP
////        );
//////        Log.d(LOG_TAG, DatabaseUtils.dumpCursorToString(lCursor));
////    }
//
//
//    public List<QObject> getAllQs() {
//        Cursor cursor = db.query(
//                QsContract.QsEntry.TABLE_NAME,
//                null,
//                null,
//                null,
//                null,
//                null,
//                QsContract.QsEntry.COLUMN_TIMESTAMP
//        );
//
//        List<QObject> lista = new ArrayList<>();
//        lista = cursorToList(cursor);
//        cursor.close();
//        return lista;
//    }
//
//
////    public Cursor getQFromMomentoDeCriacao(String momDeCriacao) {
////        String[] sArray = new String[]{momDeCriacao};
////        return db.query(
////                QsContract.QsEntry.TABLE_NAME,
////                null,
////                QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO + "=?",
////                sArray,
////                null,
////                null,
////                QsContract.QsEntry.COLUMN_TIMESTAMP
////        );
////
//////        Log.d(LOG_TAG, DatabaseUtils.dumpCursorToString(lCursor));
////    }
//
//
//    public QObject getQFromMomentoDeCriacao(String momDeCriacao) {
//        String[] sArray = new String[]{momDeCriacao};
//        Cursor cursor = db.query(
//                QsContract.QsEntry.TABLE_NAME,
//                null,
//                QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO + "=?",
//                sArray,
//                null,
//                null,
//                QsContract.QsEntry.COLUMN_TIMESTAMP
//        );
//
//        List<QObject> lista = new ArrayList<>();
//        lista = cursorToList(cursor);
//        cursor.close();
//        return lista.get(0);
//    }
//
//
//    public QObject getQFromId(int id) {
//        String[] sArray = new String[]{Integer.toString(id)};
//        Cursor cursor = db.query(
//                QsContract.QsEntry.TABLE_NAME,
//                null,
//                QsContract.QsEntry._ID + "=?",
//                sArray,
//                null,
//                null,
//                QsContract.QsEntry.COLUMN_TIMESTAMP
//        );
//
//        List<QObject> lista = new ArrayList<>();
//        lista = cursorToList(cursor);
//        cursor.close();
//        return lista.get(0);
//    }
//
//
//    public int getIdFromMomentoDeCriacao(String momDeCriacao) {
//        String[] sArray = new String[]{momDeCriacao};
//        Cursor lCursor = db.query(
//                QsContract.QsEntry.TABLE_NAME,
//                null,
//                QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO + "=?",
//                sArray,
//                null,
//                null,
//                QsContract.QsEntry.COLUMN_TIMESTAMP
//        );
//
//        lCursor.moveToFirst();
//        int id = Integer.parseInt(lCursor.getString(0));
//        lCursor.close();
//        return id;
//    }
//
//
//    public String getMomentoDeCriacaoFromId(int id) {
//        String[] sArray = new String[]{Integer.toString(id)};
//        Cursor lCursor = db.query(
//                QsContract.QsEntry.TABLE_NAME,
//                null,
//                QsContract.QsEntry._ID + "=?",
//                sArray,
//                null,
//                null,
//                QsContract.QsEntry.COLUMN_TIMESTAMP
//        );
//
//        lCursor.moveToFirst();
//        String momentoDeCriacao = lCursor.getString(lCursor.getColumnIndex(QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO));
//        lCursor.close();
//        return momentoDeCriacao;
//    }
//
//
//    private List<QObject> cursorToList(Cursor cursor) {
//        List<QObject> lista = new ArrayList<>();
////        cursor.moveToFirst();
//        cursor.moveToPosition(-1);
//        while (cursor.moveToNext()) {
//            QObject qObj = new QObject();
//            qObj.setId(cursor.getInt(cursor.getColumnIndex(QsContract.QsEntry._ID)));
//            qObj.setMomentoDeCriacao(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_MOMENTO_DE_CRIACAO)));
//            qObj.setTitulo(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_TITULO)));
//            qObj.setDescricao(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_DESCRICAO)));
//            qObj.setPriority(cursor.getInt(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_PRIORITY)));
//            qObj.setAudio(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_AUDIO)));
//            qObj.setTimeQ(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_TIME_Q)));
//            qObj.setTipoTimeQ(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_TIPO_TIME_Q)));
//            qObj.setLatLocaleQ(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_LAT_LOCALE_Q)));
//            qObj.setLonLocaleQ(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_LON_LOCALE_Q)));
//            qObj.setTipoLocaleQ(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_TIPO_LOCALE_Q)));
//            qObj.setStatus(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_STATUS)));
//            qObj.setNotificationActive(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_NOTIFICATION_ACTIVE)));
//            qObj.setTimestamp(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_TIMESTAMP)));
//            qObj.setLastModified(cursor.getString(cursor.getColumnIndex(QsContract.QsEntry.COLUMN_LAST_MODIFIED)));
//            lista.add(qObj);
//        }
////        Log.d("semCursor", "list.size é " + Integer.toString(lista.size()));
//        return lista;
//    }
//
//
//    public void closeDbAndHelper() {
//        dbHelper.close();
//        db.close();
//    }
//
//
//    public void insertFakeData() {
//        List<String> lista = new ArrayList<>();
//        lista.add("INSERT INTO 'qs' VALUES(1,'1492040039368','hoje','','Qs_1492040094732.mp4','1492040880000','único',NULL,NULL,NULL,'on',1492040039,1492644091);");
//        lista.add("INSERT INTO 'qs' VALUES(2,'1492040055019','amanhã','',NULL,'1492126440000','único',NULL,NULL,NULL,'on',1492040055,1492040055);");
//        lista.add("INSERT INTO 'qs' VALUES(3,'1492040069700','8 dias','',NULL,'1492731240000','único',NULL,NULL,NULL,'done',1492040069,1492644083);");
//        lista.add("INSERT INTO 'qs' VALUES(4,'1492040101636','','','Qs_1492040094732.mp4','1492041840000','único',NULL,NULL,NULL,'on',1492040101,1492040101);");
//        lista.add("INSERT INTO 'qs' VALUES(5,'1492040136256','','só descrição',NULL,'1492205700000','único',NULL,NULL,NULL,'on',1492040136,1492040136);");
//        lista.add("INSERT INTO 'qs' VALUES(6,'1491620400000','antes de hoje','',NULL,'1492040880000','único',NULL,NULL,NULL,'on',1492040003,1492040003);");
//        lista.add("INSERT INTO 'qs' VALUES(7,'1491361200000','alguns dias antes de hoje','tb tem descrição',NULL,'1492040810000','único',NULL,NULL,NULL,'on',1492020203,1492020203);");
//        lista.add("INSERT INTO 'qs' VALUES(8,'1491361201000','antes...','','Qs_1492040094732.mp4','1492040810000','único',NULL,NULL,NULL,'done',1492020003,1492020003);");
//        lista.add("INSERT INTO 'qs' VALUES(9,'1492040037368','hoje, done','',NULL,'1492040880000','único',NULL,NULL,NULL,'done',1492040039,1492644091);");
//        lista.add("INSERT INTO 'qs' VALUES(10,'1492040039318','sem timeq, done','',NULL,'0','único',NULL,NULL,NULL,'on',1492040439,1492644080);");
//        lista.add("INSERT INTO 'qs' VALUES(11,'1492040039328','sem timeq','',NULL,'0','único',NULL,NULL,NULL,'on',1492041039,1492644077);");
//        lista.add("INSERT INTO 'qs' VALUES(12,'1492040136356','antes de hoje com audio','','Qs_1492040094732.mp4','1491361200000','único',NULL,NULL,NULL,'on',1492047694,1492866306);");
//        lista.add("INSERT INTO 'qs' VALUES(13,'1492030136256','atrasado hoje com audio','','Qs_1492040094732.mp4','1492040880000','único',NULL,NULL,NULL,'on',1492047694,1492866306);");
//        lista.add("INSERT INTO 'qs' VALUES(14,'1492040134256','','','Qs_1492040094732.mp4','1492040880000','único',NULL,NULL,NULL,'on',1492047694,1492866306);");
//        lista.add("INSERT INTO 'qs' VALUES(15,'1492040236256','sem timeq','com descrição',NULL,'0','único',NULL,NULL,NULL,'on',1491987694,1491987694);");
//        lista.add("INSERT INTO 'qs' VALUES(16,'1492654917693','teste','',NULL,'1492741260000','único',NULL,NULL,NULL,'on',1492644117,1492644274);");
//        lista.add("INSERT INTO 'qs' VALUES(17,'1492654988484','opaopa','',NULL,'1492665780000','único',NULL,NULL,NULL,'on',1492644188,1492644275);");
//
//        for (String i : lista) {
//            db.execSQL(i);
//        }
//
//    }
//}