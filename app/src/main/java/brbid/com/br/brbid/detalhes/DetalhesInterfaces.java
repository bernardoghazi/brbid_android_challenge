package brbid.com.br.brbid.detalhes;

import brbid.com.br.brbid.model.Carro;

public interface DetalhesInterfaces {

    interface Model {
        /**
         * Atualiza o Carro que tem o id (@param id) no banco de dados com as características do @param c.
         */
        void updateCarro(int id, Carro carro);

        /**
         * Retorna todos os carros cadastrados no banco.
         */
        Carro getCarro(int id);

    }
}
