package brbid.com.br.brbid.host;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.TextView;

import brbid.com.br.brbid.R;

public class HostActivity extends AppCompatActivity {

    FragmentManager fragmentManager;
    Fragment inserirFragment;
    Fragment listarFragment;
    Fragment detalhesFragment;
    android.support.v7.widget.Toolbar toolbar;
    ImageButton inserirIb;
    ImageButton consultarIb;
    TextView tituloTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host);

        fragmentManager = getSupportFragmentManager();
        inserirFragment = fragmentManager.findFragmentById(R.id.inserirFragment);
        listarFragment = fragmentManager.findFragmentById(R.id.listarFragment);
        detalhesFragment = fragmentManager.findFragmentById(R.id.detalhesFragment);
        tituloTv = findViewById(R.id.tituloTv);
        toolbar = findViewById(R.id.toolbar_inf);
        inserirIb = findViewById(R.id.inserirIb);
        consultarIb = findViewById(R.id.consultarIb);

        FragmentTransaction transactionInicial = fragmentManager.beginTransaction();
        transactionInicial.hide(listarFragment);
        transactionInicial.hide(detalhesFragment);
        transactionInicial.show(inserirFragment);
        transactionInicial.commit();

        Toolbar myToolbar = findViewById(R.id.toolbar_sup);
        setSupportActionBar(myToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setTitulo("INSERIR CARRO");
    }

    public void setTitulo(String string) {
        tituloTv.setText(string);
    }

    @Override
    protected void onResume() {
        super.onResume();

        inserirIb.setOnClickListener(v -> {
            if (inserirFragment.isHidden()) {
                FragmentTransaction transactionInserir = fragmentManager.beginTransaction();
                transactionInserir.setCustomAnimations(R.anim.fade_in_slide_up, R.anim.fade_out_slide_up);
                transactionInserir.show(inserirFragment);
                transactionInserir.hide(listarFragment);
                transactionInserir.hide(detalhesFragment);
                transactionInserir.commit();
            }
        });

        consultarIb.setOnClickListener(v -> {
            if (listarFragment.isHidden()) {
                FragmentTransaction transactionListar = fragmentManager.beginTransaction();
                transactionListar.setCustomAnimations(R.anim.fade_in_slide_up, R.anim.fade_out_slide_up);
                transactionListar.show(listarFragment);
                transactionListar.hide(inserirFragment);
                transactionListar.hide(detalhesFragment);
                transactionListar.commit();
            }
        });
    }
}
