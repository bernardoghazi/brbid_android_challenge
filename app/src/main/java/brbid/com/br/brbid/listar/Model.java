package brbid.com.br.brbid.listar;

import android.util.Log;

import java.util.List;

import brbid.com.br.brbid.data.DAO;
import brbid.com.br.brbid.model.Carro;

public class Model implements ListarInterfaces.Model{

    private DAO dao;

    public Model(DAO dao) {
        this.dao = dao;
    }

    public List<Carro> getCarros(){
        return dao.getCarros();
    }

    @Override
    public void deleteCarro(int id) {
        dao.deleteCarro(id);
    }
}
