package brbid.com.br.brbid.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import brbid.com.br.brbid.data.CarroContract.CarroEntry;

public class DbHelper extends SQLiteOpenHelper {

    /**
     * O nome do banco de dados.
     */
    private static final String DATABASE_NAME = "brBid.db";

    /**
     * Versão do bd. Deve ser incrementada a cada mudança na eestrutura do banco.
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * Comando que cria a tabela de Carros.
     */
    private static final String SQL_CREATE_QS_TABLE = "CREATE TABLE " + CarroContract.CarroEntry.TABLE_NAME + " (" +
            CarroEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            CarroEntry.CLN_MODELO + " TEXT, " +
            CarroEntry.CLN_COR + " TEXT, " +
            CarroEntry.CLN_MARCA + " TEXT, " +
            CarroEntry.CLN_ANO + " TEXT, " +
            CarroEntry.CLN_PLACA + " TEXT, " +
            CarroEntry.CLN_TIMESTAMP + " TIMESTAMP DEFAULT (STRFTIME('%s', 'now', 'localtime')), " +
            CarroEntry.CLN_LAST_MODIFIED + " TIMESTAMP DEFAULT (STRFTIME('%s', 'now', 'localtime'))" +
            "); ";

    /**
     * Comando que cria triggers para atualizar com a data/hora atual o campo 'timestamp' (quando um Carro é inserido no db) e o campo
     * 'last_modified' (quando um carro é atualizado).
     */
    private static final String SQL_INSERT_TRIGGER = "CREATE TRIGGER if not exists LAST_UPDATED_AFTER_UPDATE "
            + "AFTER UPDATE "
            + "ON " + CarroEntry.TABLE_NAME
            + " for each row "
            + "BEGIN "
            + "UPDATE " + CarroEntry.TABLE_NAME
            + " SET " + CarroEntry.CLN_LAST_MODIFIED + " = (STRFTIME('%s', 'now', 'localtime'))" +
            " WHERE " + CarroEntry.CLN_TIMESTAMP + " = old." + CarroEntry.CLN_TIMESTAMP + ";" + " END;";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_QS_TABLE);
        sqLiteDatabase.execSQL(SQL_INSERT_TRIGGER);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}
